<?php

/*
Plugin Name: WPezPlugins: Shortcode Markup
Plugin URI: https://gitlab.com/WPezPlugins/wpez-sc-markup
Description: Use shortcodes to generate editor-proof markup + some other ez magic. Please see the README for complete details.
Version: 0.0.7
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://alchemyunited.com/
License: GPLv2 or later
Text Domain: wpez-sc-markup
*/


namespace WPezPlugins\Shortcode\Markup;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}


$str_php_ver_comp = '5.4.0';

// we reserve the right to use traits :)
if (version_compare(PHP_VERSION, $str_php_ver_comp, '<')) {
    exit(sprintf('WPezPlugins: Shortcode Markup requires PHP ' . esc_html($str_php_ver_comp) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION));
}


function requireFiles() {

    require 'app/ClassPlugin.php';

    require 'app/core/hooks/register/ClassRegisterHooks.php';

    require 'app/include/ClassShortcodeMarkup.php';
}

requireFiles();

new ClassPlugin();