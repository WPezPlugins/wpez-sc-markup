<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22-Feb-18
 * Time: 2:21 PM
 */

namespace WPezPlugins\Shortcode\Markup;


class ClassShortcodeMarkup {

    protected $_str_tag_default;

    protected $_str_bg_img_id_slug;

    protected $_str_bg_img_default_wp_size;

    protected $_arr_sizes;

    protected $_arr_sizes_settings;

    protected $_str_min_width_unit;

    protected $_arr_attrs_defaults;


    public function __construct() {

        $this->setPropertyDefaults();
    }

    /**
     *
     */
    protected function setPropertyDefaults() {

        $this->_str_tag_default            = 'div';
        $this->_str_bg_img_id_slug         = 'wpez-scmu-bg-img-';
        $this->_str_bg_img_default_wp_size = 'medium';
        $this->_arr_sizes                  = [ 'sm', 'md', 'lg', 'xl' ];
        $this->_arr_sizes_settings         = [
            'sm' => [
                'min_width' => 576,
                'wp_size'   => false
            ],
            'md' => [
                'min_width' => 768,
                'wp_size'   => false
            ],
            'lg' => [
                'min_width' => 992,
                'wp_size'   => false
            ],
            'xl' => [
                'min_width' => 1200,
                'wp_size'   => false
            ],
        ];
        $this->_str_min_width_unit         = 'px';
        $this->_arr_attrs_defaults         = [ 'id', 'class', 'role', 'aria-label' ];
    }

    /**
     * @param string $str_tag
     *
     * @return bool
     */
    public function setTagDefault( $str_tag = 'div' ) {

        if ( ! empty( esc_attr( $str_tag ) ) ) {

            $this->_str_tag_default = $str_tag;

            return true;
        }

        return false;
    }


    public function setBgImgIdSlug( $str_slug ) {

        if ( ! empty( esc_attr( $str_slug ) ) ) {

            $this->_str_bg_img_id_slug = $str_slug;

            return true;
        }

        return false;
    }

    public function setBgImgDefaultWpSize( $str_size ) {

        if ( is_string( $str_size ) ) {

            $this->_str_bg_img_default_wp_size = $str_size;

            return true;
        }

        return false;
    }

    public function setSizes( $arr_sizes = false ) {

        if ( is_array( $arr_sizes ) ) {

            $this->_arr_sizes = $arr_sizes;

            return true;
        }

        return false;
    }

    public function setSizesSettings( $arr_settings = false ) {

        if ( is_array( $arr_settings ) ) {

            $this->_arr_sizes_settings = $arr_settings;

            // note: this only means there was a set, not that the new $arr was valid
            return true;
        }

        return false;
    }

    public function updateSizesSettings( $arr_settings = false ) {

        // DOTO - check for associative array
        if ( is_array( $arr_settings ) ) {

            $this->_arr_sizes_settings = array_merge( $this->_arr_sizes_settings, $arr_settings );

            return true;
        }

        return false;
    }

    //
    public function setMinWidth( $str_size = false, $num_width = false ) {

        if ( isset( $this->_arr_sizes_settings[ $str_size ]['min_width'] ) && $num_width !== false ) {

            $this->_arr_sizes_settings[ $str_size ]['min_width'] = $num_width;

            return true;
        }

        return false;
    }

    //
    public function setWpSize( $str_size = false, $str_wp_size = false ) {

        if ( isset( $this->_arr_sizes_settings[ $str_size ]['wp_size'] ) && is_string( $str_wp_size ) ) {
            $this->_arr_sizes_settings[ $str_size ]['wp_size'] = $str_wp_size;

            return true;
        }

        return false;
    }

    public function setMinWidthUnit( $str_unit = 'px' ) {

        if ( ! empty( esc_attr( $str_unit ) ) && is_string( $str_unit ) ) {
            $this->_str_min_width_unit = $str_unit;

            return true;
        }

        return false;
    }


    protected function attsDefaults( $str = 'default' ) {


        switch ( $str ) {

            case 'bg_img_args':

                return [
                    'id_slug'         => $this->_str_bg_img_id_slug,
                    'default_wp_size' => $this->_str_bg_img_default_wp_size,
                    'min_width_unit'  => $this->_str_min_width_unit
                ];

                break;

            case'style_img_bg':

                return [
                    'img_ids' => false
                ];
                break;

            case'tag':
            default:

                return [
                    'tag'       => $this->_str_tag_default,
                    'close'     => false,
                    'oth_attrs' => '',
                ];
                break;

        }
    }


    public function shortcodeStyleBgImgsArgs( $arr_atts ) {

        $arr_sc_atts = shortcode_atts( $this->attsDefaults( 'bg_img_args' ), $arr_atts );

        if ( ! empty( esc_attr( $arr_sc_atts['id_slug'] ) ) ) {
            $this->_str_bg_img_id_slug = trim( $arr_sc_atts['id_slug'] );
        }
        $this->_str_bg_img_default_wp_size = trim( $arr_sc_atts['default_wp_size'] );

        if ( ! empty( esc_attr( $arr_sc_atts['min_width_unit'] ) ) ) {
            $this->_str_min_width_unit = trim( $arr_sc_atts['min_width_unit'] );
        }

        foreach ( $this->_arr_sizes as $size ) {

            if ( isset( $arr_atts[ trim( $size ) . '_min_width' ] ) ) {
                $this->_arr_sizes_settings[ $size ]['min_width'] = $arr_atts[ trim( $size ) . '_min_width' ];
            }

            if ( isset( $arr_atts[ $size . '_wp_size' ] ) ) {
                $this->_arr_sizes_settings[ $size ]['wp_size'] = $arr_atts[ trim( $size ) . '_wp_size' ];
            }
        }
    }


    /**
     * Given a string of img ids, this function will generate the <style> of id
     * selectors and their background-image:
     *
     * @param $arr_atts
     *
     * @return string
     */
    public function shortcodeStyleBgImgs( $arr_atts ) {

        $arr_sc_atts = shortcode_atts( $this->attsDefaults( 'style_img_bg' ), $arr_atts );

        $str_ret = '';
        if ( $arr_sc_atts['img_ids'] !== false ) {

            $arr_default = [];
            $arr_mq      = [];

            // get the img id's into an array
            $arr_ids = explode( ',', $arr_sc_atts['img_ids'] );
            foreach ( $arr_ids as $id ) {

                $mix_img_url = wp_get_attachment_image_url( $id, $this->_str_bg_img_default_wp_size );
                if ( $mix_img_url !== false ) {

                    $arr_default[] = $this->backgroundImage( $id, $mix_img_url );

                } else {
                    // no default? just continue
                    continue;
                }


                $arr_sizes = [];
                foreach ( $this->_arr_sizes as $size ) {

                    if ( ! isset( $this->_arr_sizes_settings[ $size ]['wp_size'] ) || $this->_arr_sizes_settings[ $size ]['wp_size'] === false ) {
                        continue;
                    }
                    if ( ! isset( $this->_arr_sizes_settings[ $size ]['min_width'] ) || $this->_arr_sizes_settings[ $size ]['min_width'] === false ) {
                        continue;
                    }

                    $str_wp_size = $this->_arr_sizes_settings[ $size ]['wp_size'];

                    $mix_img_url = wp_get_attachment_image_url( $id, $str_wp_size );
                    if ( $mix_img_url !== false ) {
                        $arr_mq[ $size ][] = $this->backgroundImage( $id, $mix_img_url );
                        $arr_sizes[]       = $size;
                    }
                }
            }

            if ( ! empty( $arr_default ) ) {

                // TODO - transient this so we're not always regen'ing it?
                $str_ret .= '<style>';

                $str_ret .= implode( ' ', $arr_default );

                foreach ( $arr_sizes as $size ) {
                    /*
                     move this up for we continue over any "misses" sooner rather than here
                     if ( ! isset( $this->_arr_sizes_settings[ $size ]['min_width'] ) || $this->_arr_sizes_settings[ $size ]['min_width'] === false ) {
                        continue;
                    }
                    */
                    $int_min_width = $this->_arr_sizes_settings[ $size ]['min_width'];

                    // TODO - do we need this if()? can't hurt?
                    if ( isset( $arr_mq[ $size ] ) && ! empty( $arr_mq[ $size ] ) && is_array( $arr_mq[ $size ] ) ) {
                        $str_ret .= ' @media screen and (min-width:' . esc_attr( $int_min_width . $this->_str_min_width_unit ) . '){';
                        $str_ret .= implode( ' ', $arr_mq[ $size ] );
                        $str_ret .= '} ';
                    }
                }
                $str_ret .= '</style>';
            }
        }

        return $str_ret;
    }


    protected function backgroundImage( $id, $url ) {

        return '#' . esc_attr( $this->_str_bg_img_id_slug ) . esc_attr( $id ) . '{background-image: url(' . esc_attr( $url ) . ');}';
    }


    public function shortcodeTag( $arr_atts ) {

        $arr_sc_atts = shortcode_atts( $this->attsDefaults( 'tag' ), $arr_atts );

        // TODO - validate the tag for make sure it's valid. that said, the browser ignores those anyway.
        $tag = esc_attr( $arr_sc_atts['tag'] );

        if ( $arr_sc_atts['close'] !== false || ( isset( $arr_atts[0] ) && $arr_atts[0] == 'close' ) ) {
            return '</' . $tag . ' >';
        }

        $arr_attrs = [];

        foreach ( $this->_arr_attrs_defaults as $attr ) {

            if ( isset( $arr_atts[ $attr ] ) && ! empty ( esc_attr( $arr_atts[ $attr ] ) ) ) {
                $arr_attrs[] = ' ' . esc_attr( $attr ) . '="' . esc_attr( $arr_atts[ $attr ] ) . '" ';
            }
        }

        if ( ! empty ( esc_attr( $arr_sc_atts['oth_attrs'] ) ) && strpos( $arr_sc_atts['oth_attrs'], '=' ) !== false ) {

            $arr_temp    = $this->attrsConvert( $arr_sc_atts['oth_attrs'] );
            $arr_attrs[] = implode( ' ', $arr_temp );
        }

        // TODO use an implode to pieces these pieces together
        return '<' . $tag . ' ' . implode( ' ', $arr_attrs ) . '>';

    }


    protected function attrsConvert( $arr_attrs ) {

        $arr_ret = [];
        // explode the oth_attrs string
        $arr_exps = explode( '|', $arr_attrs );

        foreach ( $arr_exps as $str_exp ) {

            // explode again, this time on the =
            $arr_temp = explode( '=', $str_exp );
            if ( isset( $arr_temp[0], $arr_temp[1] ) && ! empty( esc_attr( $arr_temp[0] ) ) && ! empty( esc_attr( $arr_temp[1] ) ) ) {
                // make the pair
                $arr_ret[] = esc_attr( $arr_temp[0] ) . '="' . esc_attr( $arr_temp[1] ) . '"';
            }
        }

        return $arr_ret;
    }
}