<?php

namespace WPezPlugins\Shortcode\Markup;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}


class ClassPlugin {

    protected $_shortcode_name;
    protected $_shortcode_name_bg_img_args;
    protected $_shortcode_name_style_bg_img;

    public function __construct() {

        $this->setPropertyDefaults();

        $this->addFilter();

        $this->addShortcode();
    }

    protected function setPropertyDefaults() {

        $this->_shortcode_name              = 'wpez_scmu';
        $this->_shortcode_name_bg_img_args  = 'wpez_scmu_style_bg_imgs_args';
        $this->_shortcode_name_style_bg_img = 'wpez_scmu_style_bg_imgs';
    }

    protected function addFilter() {

        $new_reg_hooks = new ClassRegister();

        $arr = [];
        $arr[] = (object)[
            'hook' => 'wp_default_editor',
            'component' => $this,
            'callback' => 'editorForceTextTab',
            'priority' => 75
        ];

        // Yup. You could probably argue doing this single filter this way is overkill. However, as a new tool
        // in the ez toolbox, the ClassRegisterHook() needed to be put through its paces.
        $new_reg_hooks->loadFilters($arr);
        $new_reg_hooks->doRegister();

        // add_filter( 'wp_default_editor', [ $this, 'editorForceTextTab' ], 75, 1 );
    }

    protected function addShortcode() {

        $newClassShortcodeMarkup = new ClassShortcodeMarkup();

        // TODO - add filter to allow access to this object
        add_shortcode( $this->_shortcode_name, [ $newClassShortcodeMarkup, 'shortcodeTag' ] );
        add_shortcode( $this->_shortcode_name_bg_img_args, [ $newClassShortcodeMarkup, 'shortcodeStyleBgImgsArgs' ] );
        add_shortcode( $this->_shortcode_name_style_bg_img, [ $newClassShortcodeMarkup, 'shortcodeStyleBgImgs' ] );
    }


    public function editorForceTextTab( $tab ) {

        return 'html';
    }


}