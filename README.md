## WPezPlugins: Shortcode Markup

__Use a small collection of shortcodes to define HTML markup (when the WordPress editor isn't as cooperative as you'd like). There's also some specialized CSS generation as well.__ 

If it helps, imagine the results from a visual page builder without a visual UI, but also without the underlying bloat.


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

Currently, there are three shortcodes: 

1. [wpez_scmu]
2. [wpez_scmu_style_bg_imgs_args]
3. [wpez_scmu_style_bg_imgs]
 

##### 1- [wpez_scmu]

For general markup. The (default) shortcode atts are:

- __tag__ - The default value is: tag="div". If you want the render a div you don't need to use this. 

- __close__ - When you want to close the tag (e.g., `</div>`): [wpez_scmu close="true"]. Again, div is the default value, else (e.g.) [wpez_scmu tag="span" close="true"] would close a span.

- __id__ - e.g. [wpez_scmu id="wpez-scmu-bg-img-99"] will render: `<div id="wpez-scmu-bg-img-99">`

- __class__ - e.g. [wpez_scmu tag="span" id="wpez-scmu-bg-img-99" class="my-example-class"] will render: `<span id="wpez-scmu-bg-img-99" class="my-example-class">`

- __role__ - e.g. [wpez_scmu id="wpez-scmu-bg-img-99" class="my-example-class" role="img"] will render: `<div id="wpez-scmu-bg-img-99" class="my-example-class" role="img">`

- __aria-label__ - e.g. [wpez_scmu tag="span" id="wpez-scmu-bg-img-99" class="my-example-class" role="img" aria-label="description of this bg img"] will render: `<span id="wpez-scmu-bg-img-99" class="my-example-class" role="img" aria-label="description of this bg img">`

- __oth_attrs__ - Short for: other attributes. These would be (global) attributes other than the previous four listed. The delimiter is a | (aka pipe). For the sake of a simple example, we'll define an id and class via oth_attrs. e.g., [wpez_scmu tag="span" oth_attrs="id=wpez-scmu-bg-img-99|class=my-example-class] will render: `<span id="wpez-scmu-bg-img-99" class="my-example-class">`


##### 2- [wpez_scmu_style_bg_imgs_args]

When you're using the [wpez_scmu_style_bg_imgs] shortcode (see below), the [wpez_scmu_style_bg_imgs_args] allows you to customize (read: override and/or define) the plugin's defaults. Yes, that's correct. A shortcode to configure your shortcode. 

The (default) shortcode atts:

- __id_slug__ - The default value is: "wpez-scmu-bg-img-". This slug is used by [wpez_scmu_style_bg_imgs] when it creates the CSS we're asking it to render. See the example under [wpez_scmu_style_bg_imgs_args].  

- __default_wp_size__ - The WordPress image size that - if nothing else - [wpez_scmu_style_bg_imgs] will use to generate the `<style>...</style>`. Again, the example below is going to help. The default value for this atts is: "medium".

- __min_width_unit__ - The unit of measure for min-width of the @media screen and (min-width:...).  The default value is: "px". If you prefer "em" then (e.g.) [wpez_scmu_style_bg_imgs_args min_width_unit="em"]. Note: If you switch from "px" you'll have to also change the min_width value(s) (which will be explained shortly).

That is, in summary,  [wpez_scmu_style_bg_imgs_args] is actually: [wpez_scmu_style_bg_imgs_args id_slug="wpez-scmu-bg-img-" default_size="medium" min_width_unit="px"]

> --
>
>__WARNING!!!__
> 
>__This next bit is slightly complicated. Ideally, once you get through the example (below), this should make more sense.__ 
>
>__In fact, have a quick look at the example (below) and come back.__ 
>
> --

The warning aside, let's set up the context.

Ultimately, we are going to have the [wpez_scmu_style_bg_imgs] shortcore generate a chunk of `<style>...</style>` for us; for a set of id's and their background-image (property). 


The added bonus is, rather than having a single (background) image associated with an id, we can have different (WP image) sizes of the same image associated with an id, based on breakpoint.

There are four default breakpoints. The code looks something like this:

```
$_sizes_settings = [

  'sm' => [
    'min_width' => 576,
    'wp_size'   => false
  ],
  'md' => [
    'min_width' => 768,
    'wp_size'   => false
  ],
  'lg' => [
    'min_width' => 992,
    'wp_size'   => false
  ],
  'xl' => [
    'min_width' => 1200,
    'wp_size'   => false
  ],
  
]
```

There is no 'xs' defined because in a mobile-first world that is defined by the default_size atts mentioned above. 

The plugin will only use the sizes (i.e., 'sm', 'md', etc.) that have a "wp_size" defined; and false is obviously not a size. 

For example, if you wanted the 'lg' breakpoint to use the large WP image size then: [wpez_scmu_style_bg_imgs_args lg_wp_size="large"]. If you also wanted to change the 'lg' min_width breakpoint: [wpez_scmu_style_bg_imgs_args lg_min_width="800" lg_wp_size="large"]. 
          ``

##### 3 - [wpez_scmu_style_bg_imgs]

__Example 1__

Let's say this is your markup:

```
<div class="my-demo-wrapper">
<div id="wpez-scmu-bg-img-3184" class="bg-img"></div>
<div id="wpez-scmu-bg-img-3210" class="bg-img"></div>
</div>
```

Please Note:

- This is a quick & dirty example. Your actual markup is likely to be different.
- The id slug - i.e., the bit before the img id (e.g., 3184) - MUST match id_slug (discussed above). Again, the default is: "wpez-scmu-bg-img-".
- _IMPORTANT_ - It's up to you to define the properties of "bg-img" class in your own CSS. For example, the height property, or padding-bottom if you're trying to maintain a certain aspect ratio. This shortcode will only render CSS for the id's and their associated background-images.

And let's say these are the shortcodes in your post/page. 
```
[wpez_scmu_style_bg_imgs_args lg_wp_size="large"]
[wpez_scmu_style_bg_imgs img_ids="3184,3210"]
```


The [wpez_scmu_style_bg_imgs] shortcode will render this `<style>` when your post/page is displayed:

```
<style>
#wpez-scmu-bg-img-3184{background-image: url(https://demo.com/wp-content/uploads/2018/04/demo-img3184-size-medium.jpg);} 
#wpez-scmu-bg-img-3210{background-image: url(https://demo.com/wp-content/uploads/2018/04/demo-img3210-size-medium.jpg);} 
@media screen and (min-width:992px){
#wpez-scmu-bg-img-3184{background-image: url(https://demo.com/wp-content/uploads/2018/04/demo-img3184-size-large.jpg);} 
#wpez-scmu-bg-img-3210{background-image: url(https://demo.com/wp-content/uploads/2018/04/demo-img3184-size-large.jpg);}
}
</style>
```

Please Note:

- The image file names were altered, slightly. The usual width x height bit that WP adds was removed.
- The actual code produced by the shortcode does _not_ have returns. 


### FAQ

__1 - Why?__

Mainly because I wanted to use CSS background images (read: background-size: cover, as well as have better control over aspect ratio of the div containing the background image). This is what [wpez_scmu_style_bg_imgs_args] and [wpez_scmu_style_bg_imgs] do. 

Closing the loop and doing markup via a shortcode  - [wpez_scmu] - seemed worth the extra effort. 

__2 - Why have id, class, role and aria-label when they can be done via oth_attrs?__

Good question. The answer is, mainly for readability. A long string delimited with a | can be difficult to read. These were the four attributes I was the most concerned about, so they were broken out on their own.  

__3 - What are role and aria-label?__

ARIA is the acronym for Accessible Rich Internet Applications (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA)

Role (http://www.webteacher.ws/2010/10/14/aria-roles-101/) and aria-label (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-label_attribute) are part of that standard. 

__4 - Are the default breakpoints from Bootstrap 4?__

Yes, they are.

__5 - What's a breakpoint?__

Not to offend you but if that's the case then this plugin probably isn't for you. I kindly suggest you speak to your WP developer about it. 

__6 - I'm not very comfortable with HTML and CSS, can you help me?__

For a fee? Sure. Else, again I suggest you speak with your developer. They'll be in a better position to decide if this plugin is a good fit for your WordPress-based project.

Without a firm understanding of CSS this plugin will likely be difficult for you to use. 

__7 - The primary class (app/include/ClassShortcodeMarkup.php) is flexible, can we use it for our own plugin?__

Yes.

_However..._ 

You must change the namespace. Please be mindful of the fact that someone else might also create a plugin with this class. 

That said, if you're 100% certain you're not going to release your plugin into the wild (e.g., the WP.org repo, GitHub, GitLab, etc.) or give it to anyone who might, then the namespace isn't a significate issue. 

When in doubt, be smart & safe, and fork the class and change the namespace. 


### SEE ALSO

- __Accessibility and Background Images__

  http://davidmacd.com/blog/css-background-images.html

- __Aspect Ratio Boxes__

  https://css-tricks.com/aspect-ratio-boxes/
 
 
### TODO

- Update namespaces to be ez-esque
- Add filters to make the various settings in the primary class accessible via the plugin
- Additional examples for [wpez_scmu_style_bg_imgs].
- Lazyload for the div's that are assigned background images. As an additional plugin? 
- Autoloader


### CHANGE LOG

__-- 0.0.8__

- CHANGED - Restructed folder core/registerhooks to core/hooks/register

__-- 0.0.7__

- FIXED - Property / var wasn't name properly (so outputting markup was broken)

__-- 0.0.6__

- CHANGED - Reorg'ed structure, as "inspired by" Devin Vinson's boilerplate (https://github.com/DevinVinson/WordPress-Plugin-Boilerplate)
- CHANGED - Did some property / var renaming 
- ADDED - app/core/ ClassAddHooks

__-- 0.0.5__

- CHANGED - Method names and assigning property default values 


